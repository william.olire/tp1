package fr.uavignon.recyclerviewdemo;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import data.Country;
import static data.Country.countries;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView nom ,capitale;
        private ImageView drapeau ;




        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nom = itemView.findViewById(R.id.item_detail);
            capitale = itemView.findViewById(R.id.item_title);
            drapeau = itemView.findViewById(R.id.item_image);



            int position = getAdapterPosition();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    /// Implementation with bundle
                    //Bundle bundle = new Bundle();
                    //bundle.putInt("numChapter", position);
                    //Navigation.findNavController(v).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);


                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment(position) ;
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nom.setText(countries[position].getName());
        holder.capitale.setText(countries[position].getCapital());

        Context c=holder.drapeau.getContext();
        String uri = countries[position].getImgUri();
        holder.drapeau.setImageResource(c.getResources().getIdentifier(uri,null,c.getPackageName()));





    }

    @Override
    public int getItemCount() {
        return countries.length;
    }


}
