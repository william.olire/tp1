package fr.uavignon.recyclerviewdemo;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import data.Country;

public class DetailFragment extends Fragment {
    private TextView pays, Capitale, langue, monnaie, population, superficie;
    private ImageView Drapeau;

    public static final String TAG = "DetailFragment";
    TextView textView;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        int position = args.getKeyChapterId();

        pays =view.findViewById(R.id.pais);
        Capitale =view.findViewById(R.id.capital);
        langue =view.findViewById(R.id.idioma);
        monnaie =view.findViewById(R.id.argent);
        population =view.findViewById(R.id.population);
        superficie =view.findViewById(R.id.Superficie);
        Drapeau=view.findViewById(R.id.image);



        pays.setText(Country.countries[position].getName());
        superficie.setText(String.valueOf(Country.countries[position].getArea())+" km2");
        langue.setText(Country.countries[position].getLanguage());
        monnaie.setText(Country.countries[position].getCurrency());
        population.setText(String.valueOf(Country.countries[position].getPopulation()));
        Capitale.setText(Country.countries[position].getCapital());
        String image = Country.countries[position].getImgUri();
        Context c = Drapeau.getContext();
        Drapeau.setImageResource(c.getResources().getIdentifier(image,null,c.getPackageName()));



        view.findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController( DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }




}